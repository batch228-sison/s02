console.log("S02 Activity")

// ======
// =QUIZ=
// ======
// What is the term given to unorganized code that's very hard to work with?
console.log("spaghetti code")

// How are object literals written in JS?
console.log("curly braces {}")

// What do you call the concept of organizing information and functionality to belong to an object?
console.log("Encapsulation")

// If the studentOne object has a method named enroll(), how would you invoke it?
console.log("studentOne.enroll()")

// True or False: Objects can have objects as properties.
console.log(true)

// What is the syntax in creating key-value pairs?
console.log("const object = {key:value}")

// True or False: A method can have no parameters and still work.
console.log(true)

// True or False: Arrays can have objects as elements.
console.log(true)

// True or False: Arrays are objects.
console.log(true)

// True or False: Objects can have arrays as properties.
console.log(true)


// Coding Activity:

// 1. Translate the other students from our boilerplate code into their own respective objects.

// 2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

// 3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.

// 4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).
let studentOne = {
    name: "John",
    email: "john@mail.com",
    grades: [89, 84, 78, 88],
    login() { console.log(`${this.name} has logged in`) },
    logout() { console.log(`${this.name} has logged out`) },
    listGrades() { console.log(`${this.name}'s grades are ${this.grades}`) },
    computeAve() {
        return (this.grades.reduce((a, b) => a + b) / this.grades.length)
    },
    willPass() {
        return this.computeAve() >= 85
    },
    willPassWithHonors() {
        if (this.computeAve() >= 90) return true
        if (this.computeAve() >= 85 && this.computeAve() < 90) return false
        else return undefined
    }
}

let studentTwo = {
    name: "Joe",
    email: "joe@mail.com",
    grades: [78, 82, 79, 85],
    login() { console.log(`${this.name} has logged in`) },
    logout() { console.log(`${this.name} has logged out`) },
    listGrades() { console.log(`${this.name}'s grades are ${this.grades}`) },
    computeAve() {
        return (this.grades.reduce((a, b) => a + b) / this.grades.length)
    },
    willPass() {
        return this.computeAve() >= 85
    },
    willPassWithHonors() {
        if (this.computeAve() >= 90) return true
        if (this.computeAve() >= 85 && this.computeAve() < 90) return false
        else return undefined
    }
}

let studentThree = {
    name: "Jane",
    email: "jane@mail.com",
    grades: [87, 89, 91, 93],
    login() { console.log(`${this.name} has logged in`) },
    logout() { console.log(`${this.name} has logged out`) },
    listGrades() { console.log(`${this.name}'s grades are ${this.grades}`) },
    computeAve() {
        return (this.grades.reduce((a, b) => a + b) / this.grades.length)
    },
    willPass() {
        return this.computeAve() >= 85
    },
    willPassWithHonors() {
        if (this.computeAve() >= 90) return true
        if (this.computeAve() >= 85 && this.computeAve() < 90) return false
        else return undefined
    }
}

let studentFour = {
    name: "Jessie",
    email: "jessie@mail.com",
    grades: [91, 89, 92, 93],
    login() { console.log(`${this.name} has logged in`) },
    logout() { console.log(`${this.name} has logged out`) },
    listGrades() { console.log(`${this.name}'s grades are ${this.grades}`) },
    computeAve() {
        return (this.grades.reduce((a, b) => a + b) / this.grades.length)
    },
    willPass() {
        return this.computeAve() >= 85
    },
    willPassWithHonors() {
        if (this.computeAve() >= 90) return true
        if (this.computeAve() >= 85 && this.computeAve() < 90) return false
        else return undefined
    }
}
console.log(studentOne.computeAve())
console.log(studentTwo.computeAve())
console.log(studentThree.computeAve())
console.log(studentFour.computeAve())

// 5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.
// 6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.
// 7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.
// 8. Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.
// 9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

let classOf1A = {
    students: [studentOne, studentTwo, studentThree, studentFour],
    countHonorStudent() {
        let count = 0
        this.students.forEach(student => {
            if (student.willPassWithHonors() === true) count++
        })
        return count
    },
    honorsPercentage() {
        let percent = (this.countHonorStudent() / this.students.length) * 100
        console.log(`${percent}%`)
    },
    retrieveHonorStudentInfo() {
        let honorStudents = []
        this.students.forEach(student => {
            if (student.willPassWithHonors()) {
                honorStudents.push({
                    avgGrade: student.computeAve(),
                    email: student.email
                })
            }
        })
        return honorStudents
    },
    sortHonorStudentsByGradesDesc() {
        return this.retrieveHonorStudentInfo().sort((a, b) => b.avgGrade - a.avgGrade)
    }
}


